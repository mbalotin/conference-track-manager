package com.dbc.ctm;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.dbc.ctm.conference.Conference;
import com.dbc.ctm.conference.talk.Talk;
import com.dbc.ctm.conference.talk.io.TalkInputHandler;
import com.dbc.ctm.exceptions.InvalidTalkLenghtException;
import com.dbc.ctm.exceptions.NoTalkFoundException;

public class Main {

	public static void main(String[] args) {

		// Validates the arguments input
		if (args.length != 1) {
			System.out.println();
			if (args.length == 0) {
				printErrorMessage("Missing input file, inform the Tasks file");
			} else {
				/*
				 * As the output will be printed I avoided to handle more than one File, but
				 * nothing a loop couldn't handle.
				 */
				printErrorMessage("Too-many parameters informed, requires only one input file");
			}
			return;
		}

		try {
			// Checks the file existence
			File inputFile = new File(args[0]);
			List<Talk> loadedTalks = extractTalks(inputFile);
			// Sort the talks from longest to shortest
			loadedTalks.sort((t1, t2) -> Integer.compare(t2.getLegth(), t1.getLegth()));
			// Instantiate the Conference and schedule the Talks
			Conference conference = new Conference();
			conference.organizeTracks(loadedTalks);
			// Print the output Schedule
			System.out.println(conference.printSchedules());
		} catch (FileNotFoundException ex) {
			printErrorMessage(String.format("File '%s' not found.", args[0]));
		} catch (IOException ex) {
			printErrorMessage(String.format("Can't open file '%s' for reading.", args[0]));
		} catch (NoTalkFoundException e) {
			printErrorMessage("No Talk found in the informed input file.");
		} catch (InvalidTalkLenghtException ex) {
			printErrorMessage(ex.getMessage());
		}
	}

	/**
	 * Read a given {@link File} lines and covert then to a {@link List} of
	 * {@link Talk}s
	 * 
	 * @param inputFile
	 *            input {@link File}
	 * @return {@link List} of {@link Talk}s
	 * @throws IOException
	 *             if reading permission is not granted
	 * @throws InvalidTalkLenghtException
	 *             if any talk on the file has an invalid time constraint
	 * @throws NoTalkFoundException
	 */
	private static List<Talk> extractTalks(File inputFile)
			throws IOException, InvalidTalkLenghtException, NoTalkFoundException {
		// Read file line-by-line
		try (BufferedReader buff = new BufferedReader(new FileReader(inputFile))) {
			List<Talk> result = new ArrayList<>();
			String line = "";
			// For every line in the File, convert to a Talk object
			while ((line = buff.readLine()) != null) {
				result.add(TalkInputHandler.stringToTalk(line));
			}
			if (result.isEmpty()) {
				throw new NoTalkFoundException();
			}
			return result;
		}
	}

	/**
	 * Simple Error Handling method
	 * 
	 * @param msg
	 */
	private static void printErrorMessage(String msg) {
		System.out.println("Conference Track Manager -- Error");
		System.out.println("  > " + msg);
	}

}
