package com.dbc.ctm.conference;

import java.util.ArrayList;
import java.util.List;

import com.dbc.ctm.conference.talk.Talk;
import com.dbc.ctm.conference.track.Track;
import com.dbc.ctm.exceptions.InvalidTalkLenghtException;

/**
 * Class responsible for organizing the {@link Talk}s and {@link Track}s
 * 
 * @author mbalotin
 *
 */
public class Conference {

	private List<Track> tracks;

	public List<Track> getTracks() {
		return tracks;
	}

	public Conference() {
		tracks = new ArrayList<>();
	}

	/**
	 * Generate the necessary {@link Track}s on the {@link Conference} to allocate
	 * all given {@link Talk}s.
	 * 
	 * @param talks
	 *            All available {@link Talk}s.
	 * @throws InvalidTalkLenghtException
	 *             If any {@link Talk} in the informed set has a length bigger than
	 *             the supported by a {@link Track} period.
	 */
	public void organizeTracks(List<Talk> talks) throws InvalidTalkLenghtException {
		for (Talk talk : talks) {
			if (!schedule(talk)) {
				// No capable Track found, so add a new one to handle the Talk
				Track newTrack = new Track();
				newTrack.schedule(talk);
				tracks.add(newTrack);
			}
		}
	}

	/**
	 * Schedules a given {@link Talk} into any existing {@link Track}.
	 * 
	 * @param talk
	 * @return true - if successfully schedules the {@link Talk} </br>
	 *         false - otherwise
	 * @throws InvalidTalkLenghtException
	 *             If the {@link Talk} length exceeds any {@link Track}s period size
	 */
	private boolean schedule(Talk talk) throws InvalidTalkLenghtException {
		// try to allocate a Talk into any existing track
		for (Track track : tracks) {
			if (track.schedule(talk)) {
				// if successfully schedules the talk into a track, continue the external talk
				// loop to schedule the next one.
				return true;
			}
		}
		return false;
	}

	/**
	 * Print the full schedule from all {@link Track}s of the {@link Conference}.
	 * 
	 * @return a {@link String} containing information to be printed.
	 */
	public StringBuilder printSchedules() {
		StringBuilder result = new StringBuilder();
		int i = 1;
		for (Track track : tracks) {
			result.append(System.getProperty("line.separator"));
			result.append(String.format("Track %d:", i++));
			result.append(System.getProperty("line.separator"));
			result.append(track.printSchedule());
		}
		return result;
	}
}