package com.dbc.ctm.conference.talk.io;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.dbc.ctm.conference.talk.Talk;
import com.dbc.ctm.conference.talk.io.TalkInputHandler;
import com.dbc.ctm.exceptions.InvalidTalkLenghtException;

public class TalkInputHandlerTest {

	static String TALK_TITLE = "Lua for the Masses";
	static int TALK_TIME_MIN = 30;
	static String TALK_TIME_LIGHTNING = "lightning";

	static String VALID_TALK_INPUT_I = TALK_TITLE + " " + TALK_TIME_MIN + "min";
	static String VALID_TALK_INPUT_II = TALK_TITLE + " " + TALK_TIME_LIGHTNING;

	static String INVALID_TALK_INPUT_NO_TIME = TALK_TITLE;
	static String INVALID_TIME_INPUT_I = TALK_TITLE + " 5m0in";
	static String INVALID_TIME_INPUT_II = TALK_TITLE + "50min";
	static String INVALID_TIME_INPUT_NEGATIVE = TALK_TITLE + " -50min";

	static Talk VALID_TALK_I;
	static Talk VALID_TALK_II;

	@Before
	public void setUp() {
		VALID_TALK_I = new Talk(TALK_TITLE, 30);
		VALID_TALK_II = new Talk(TALK_TITLE, 5);
	}

	@Test
	public void validInput_Minutes_Test() throws InvalidTalkLenghtException {
		Talk talk = TalkInputHandler.stringToTalk(VALID_TALK_INPUT_I);
		assertEquals(VALID_TALK_I, talk);
		assertEquals(TALK_TIME_MIN, talk.getLegth());
	}

	@Test
	public void validInput_Lightning_Test() throws InvalidTalkLenghtException {
		Talk talk = TalkInputHandler.stringToTalk(VALID_TALK_INPUT_II);
		assertEquals(VALID_TALK_II, talk);
		assertEquals(5, talk.getLegth());
	}

	@Test(expected = InvalidTalkLenghtException.class)
	public void invalidInput_NoTime_Test() throws InvalidTalkLenghtException {
		TalkInputHandler.stringToTalk(INVALID_TALK_INPUT_NO_TIME);
	}

	@Test(expected = InvalidTalkLenghtException.class)
	public void invalidInput_InvalidLastText_Test() throws InvalidTalkLenghtException {
		TalkInputHandler.stringToTalk(INVALID_TIME_INPUT_I);
	}

	@Test(expected = InvalidTalkLenghtException.class)
	public void invalidInput_MissingSpace_Test() throws InvalidTalkLenghtException {
		TalkInputHandler.stringToTalk(INVALID_TIME_INPUT_II);
	}

	@Test(expected = InvalidTalkLenghtException.class)

	public void invalidInput_NegativeTime_Test() throws InvalidTalkLenghtException {
		TalkInputHandler.stringToTalk(INVALID_TIME_INPUT_NEGATIVE);
	}

	@Test
	public void talkObjectOutputString_Lightning_Test() throws InvalidTalkLenghtException {
		String output = TalkInputHandler.talkToString(VALID_TALK_I);
		assertEquals(VALID_TALK_INPUT_I, output);
	}

	@Test
	public void talkObjectOutputString_NotLightningLength_Test() throws InvalidTalkLenghtException {
		String output = TalkInputHandler.talkToString(VALID_TALK_II);
		assertEquals(VALID_TALK_INPUT_II, output);
	}

}
