package com.dbc.ctm.conference.track;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.dbc.ctm.conference.talk.Talk;
import com.dbc.ctm.exceptions.InvalidTalkLenghtException;

public class TrackTest {

	static String TALK_DESCRIPTION = "Talk Description";
	static int TALK_LENGTH = 60;
	static int OVER_TALK_LENGTH = 13 * 60;

	Track track;

	@Before
	public void setUp() {
		track = new Track();
	}

	@Test
	public void validTalkAndEmptyTrack_SuccessfullySchedule_Test() throws InvalidTalkLenghtException {
		Talk talk = new Talk(TALK_DESCRIPTION, TALK_LENGTH);
		assertTrue(track.schedule(talk));
	}

	@Test
	public void validTalkAndFullTrack_UnableToSchedule_Test() throws InvalidTalkLenghtException {
		givenThatTrackIsFull();
		Talk talk = new Talk(TALK_DESCRIPTION, TALK_LENGTH);
		assertFalse(track.schedule(talk));
	}

	private void givenThatTrackIsFull() throws InvalidTalkLenghtException {
		Talk talk = new Talk(TALK_DESCRIPTION, Track.MORNING_SESSION_LENGTH);
		assertTrue(track.schedule(talk));
		talk = new Talk(TALK_DESCRIPTION, Track.AFTERNOON_SESSION_LENGTH);
		assertTrue(track.schedule(talk));
	}

	@Test(expected = InvalidTalkLenghtException.class)
	public void overlimitTask_ThrowsLimitException_Test() throws InvalidTalkLenghtException {
		Talk talk = new Talk(TALK_DESCRIPTION, OVER_TALK_LENGTH);
		track.schedule(talk);
	}

	@Test
	public void fullTrack_ScheduledNetworkingAt5PM_Test() throws InvalidTalkLenghtException {
		givenThatTrackIsFull();
		assertTrue(track.printSchedule().toString()
				.contains(String.format("%02d:%02d%s %s", 5, 0, IntervalEnum.PM, Track.NETWORK_EVENT_DESCRIPTION)));
	}

	@Test
	public void notFullTrack_ScheduledNetworkingAt4PM_Test() throws InvalidTalkLenghtException {
		assertTrue(track.printSchedule().toString()
				.contains(String.format("%02d:%02d%s %s", 4, 0, IntervalEnum.PM, Track.NETWORK_EVENT_DESCRIPTION)));
	}

	@Test
	public void nearFullTrack_ScheduledNetworkingAt430PM_Test() throws InvalidTalkLenghtException {
		int scheduleMinutes = 30;
		Talk talk = new Talk(TALK_DESCRIPTION, Track.MORNING_SESSION_LENGTH);
		assertTrue(track.schedule(talk));
		talk = new Talk(TALK_DESCRIPTION, Track.AFTERNOON_SESSION_LENGTH - scheduleMinutes);
		assertTrue(track.schedule(talk));
		assertTrue(track.printSchedule().toString().contains(
				String.format("%02d:%02d%s %s", 4, scheduleMinutes, IntervalEnum.PM, Track.NETWORK_EVENT_DESCRIPTION)));
	}
}
