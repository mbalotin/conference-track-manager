package com.dbc.ctm.exceptions;

/**
 * Exception regarding invalid time length input informations.
 * 
 * @author mbalotin
 *
 */
public class InvalidTalkLenghtException extends Exception {

	public InvalidTalkLenghtException(String msg) {
		super(msg);
	}

}
