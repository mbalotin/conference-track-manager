package com.dbc.ctm.conference.talk;

import java.util.Objects;

/**
 * Clas representing a Talk
 * 
 * @author mbalotin
 *
 */
public class Talk {

	private String title;
	private int legth;

	public Talk(String title, int length) {
		this.setTitle(title);
		this.setLegth(length);
	}

	public int getLegth() {
		return legth;
	}

	public void setLegth(int legth) {
		this.legth = legth;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Talk)
			return this.title.equals(((Talk) obj).title);
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.title, this.legth);
	}

}
