package com.dbc.ctm.exceptions;

import com.dbc.ctm.conference.track.Session;

/**
 * Thrown if a Session schedule period is invalid.
 * 
 * @author mbalotin
 * @see Session#printSchedule(int, int,
 *      com.dbc.ctm.conference.track.IntervalEnum)
 */
public class InvalidTimeException extends RuntimeException {

	public InvalidTimeException(String msg) {
		super(msg);
	}
}
