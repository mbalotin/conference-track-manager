package com.dbc.ctm.conference.track;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.dbc.ctm.conference.talk.Talk;
import com.dbc.ctm.exceptions.InvalidTimeException;

public class SessionTest {

	static int SESSION_LIMIT = 10;
	static String TALK_DESCRIPTION = "Talk Description";

	Session session;

	@Before
	public void setUp() {
		session = new Session(SESSION_LIMIT);
	}

	@Test
	public void noTalk_NoOccupation_Test() {
		assertEquals(0, session.getOccupation());
	}

	@Test
	public void schedulableTalk_SessionOccupationEqualToTalkLength_Test() {
		Talk talk = new Talk(TALK_DESCRIPTION, SESSION_LIMIT / 2);
		assertTrue(session.addTalk(talk));
		assertEquals(talk.getLegth(), session.getOccupation());
	}

	@Test
	public void multiSchedulableTalks_SessionOccupationEqualToTalksLength_Test() {
		int talkLength = SESSION_LIMIT / 4;
		Talk talk = new Talk(TALK_DESCRIPTION, talkLength);
		for (int i = 0; i < 4; i++) {
			assertTrue(session.addTalk(talk));
		}
		assertEquals(talkLength * 4, session.getOccupation());
	}

	@Test
	public void singleOverlimitTalks_NotScheduled_Test() {
		Talk talk = new Talk(TALK_DESCRIPTION, SESSION_LIMIT + 1);
		assertFalse(session.addTalk(talk));
		assertEquals(0, session.getOccupation());
	}

	@Test
	public void multiLimitSchedulableTalks_NotScheduledOvers_Test() {
		Talk talk = new Talk(TALK_DESCRIPTION, SESSION_LIMIT);
		assertTrue(session.addTalk(talk));
		assertFalse(session.addTalk(talk));
		assertEquals(SESSION_LIMIT, session.getOccupation());
	}

	@Test
	public void multiSchedulableTalks_NotScheduledOvers_Test() {
		int talkLength = SESSION_LIMIT / 4;
		Talk talk = new Talk(TALK_DESCRIPTION, talkLength);
		for (int i = 0; i < 4; i++) {
			assertTrue(session.addTalk(talk));
		}
		Talk extraOVERTalk = new Talk(TALK_DESCRIPTION, SESSION_LIMIT);
		assertFalse(session.addTalk(extraOVERTalk));
		assertEquals(talkLength * 4, session.getOccupation());
	}

	@Test(expected = InvalidTimeException.class)
	public void invalidStartingHour_throwInvalidTime_Test() {
		session.printSchedule(99, 0, IntervalEnum.PM);
	}

	@Test(expected = InvalidTimeException.class)
	public void startOvercomeOccupation_throwInvalidTime_Test() {
		Talk talk = new Talk(TALK_DESCRIPTION, SESSION_LIMIT);
		assertTrue(session.addTalk(talk));
		session.printSchedule(11, 59, IntervalEnum.PM);
	}

	@Test
	public void emptyTalks_emptyString_Test() {
		assertEquals(0, session.printSchedule(11, 59, IntervalEnum.PM).length());
	}

	@Test
	public void schedulableTalk_PrintSchedule_Test() {
		Talk talk = new Talk(TALK_DESCRIPTION, SESSION_LIMIT);
		assertTrue(session.addTalk(talk));
		assertNotEquals(0, session.printSchedule(0, 0, IntervalEnum.PM).length());
	}

}
