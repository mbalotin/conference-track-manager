package com.dbc.ctm.conference.track;

import java.util.ArrayList;
import java.util.List;

import com.dbc.ctm.conference.talk.Talk;
import com.dbc.ctm.conference.talk.io.TalkInputHandler;
import com.dbc.ctm.exceptions.InvalidTimeException;

/**
 * A Set of {@link Talk}s within a time limit.
 * 
 * @author mbalotin
 *
 */
public class Session {

	private List<Talk> talks;
	/**
	 * Session limit, in minutes
	 */
	private int limit;

	/**
	 * @param limit
	 *            Session limit, in minutes.
	 */
	public Session(int limit) {
		this.limit = limit;
		talks = new ArrayList<>();
	}

	/**
	 * @return Current occupation of a Session, in minutes
	 */
	public int getOccupation() {
		return talks.stream().mapToInt(t -> t.getLegth()).sum();
	}

	/**
	 * Schedules a {@link Talk} into the {@link Session}.
	 * 
	 * @param talk
	 *            {@link Talk} to be scheduled.
	 * @return true - if successfully scheduled </br>
	 *         false - if there is no free occupation to schedule.
	 */
	public boolean addTalk(Talk talk) {
		if (talk.getLegth() > limit - getOccupation()) {
			return false;
		} else {
			talks.add(talk);
			return true;
		}
	}

	/**
	 * Prints the full allocation of Talks for the {@link Session}.
	 * 
	 * @throws InvalidTimeException
	 *             If the given hour/minute is invalid. </br>
	 *             - Not between 1-12 and 0-60 </br>
	 *             - Starting time plus the Session Occupation exceeds the mark of
	 *             12 hours.
	 * @return a {@link StringBuilder} containing the concatenated information to be
	 *         printed. Decided to return the String Build due this information can
	 *         be used up in aggregation classes without wasting memory in
	 *         unnecessary String formation until final output.
	 */
	public StringBuilder printSchedule(int startHour, int startMinute, IntervalEnum interval)
			throws InvalidTimeException {
		StringBuilder result = new StringBuilder();
		// Test if the hour/minute input are invalid.
		if (!(startHour >= 0 && startHour < 12 && startMinute >= 0 && startMinute < 60)) {
			throw new InvalidTimeException(
					"Starting hour must be between 1-12, and starting minutes must be between 0-60.");
		}
		// Test if occupation exceeds the informed period to print
		if ((startHour * 60 + startMinute + getOccupation()) > 12 * 60) {
			throw new InvalidTimeException("Informed starting time can't scheedule into a valid interval.");
		}
		// control variables for time
		int hour = startHour;
		int minute = startMinute;
		for (Talk talk : talks) {
			// Print the current Scheduled time
			result.append(String.format("%02d:%02d%s %s", hour, minute, interval, TalkInputHandler.talkToString(talk)));
			result.append(System.getProperty("line.separator"));
			// Calculate the next talk's hour/minute
			minute += talk.getLegth();
			hour += minute / 60;
			minute = minute % 60;
		}
		return result;
	}

}
