package com.dbc.ctm.conference.talk.io;

import com.dbc.ctm.conference.talk.Talk;
import com.dbc.ctm.exceptions.InvalidTalkLenghtException;

/**
 * Class responsible to handle different inputs into a {@link Talk} object.
 * 
 * @author mbalotin
 *
 */
public class TalkInputHandler {

	private static String LIGHTNING = "lightning";
	private static String MIN = "min";

	private static int LIGHTNING_LENGTH = 5;

	/**
	 * Converts an input String input into a {@link Talk}.
	 * 
	 * @param input
	 *            Must follow the specifications </br>
	 *            <title><length-minutes>min (for general {@link Talk}s, where
	 *            <length-minutes> greater than zero. )</br>
	 *            <title>lighting (for a default 5 minutes talk)
	 * @return a {@link Talk} object
	 * @throws InvalidTalkLenghtException
	 *             If defined {@link Talk} length is invalid.
	 */
	public static Talk stringToTalk(String input) throws InvalidTalkLenghtException {
		String[] split = input.trim().split(" ");

		String timeEntry = split[split.length - 1];
		String title = input.substring(0, input.length() - timeEntry.length() - 1);

		int length = 0;
		if (LIGHTNING.equals(timeEntry)) {
			length = LIGHTNING_LENGTH;
		} else if (timeEntry.endsWith(MIN)) {
			timeEntry = timeEntry.replace(MIN, "");
			try {
				length = Integer.valueOf(timeEntry);
			} catch (NumberFormatException ex) {
				throw new InvalidTalkLenghtException(String.format(
						"%s is not a valid length for a Task, use either 'lightning' or '<length>min'.", timeEntry));
			}
			if (length <= 0) {
				throw new InvalidTalkLenghtException(
						String.format("%d is not a valid length for a Task. Length must be greatter than 0.", length));
			}
		} else {
			throw new InvalidTalkLenghtException(String.format(
					"%s is not a valid time entry for a Task, use either 'lightning' or '<length>min'.", timeEntry));
		}
		return new Talk(title, length);
	}

	/**
	 * Output from {@link Talk} to {@link String}.
	 * 
	 * @param talk
	 *            {@link Talk}
	 * @return Description {@link String}
	 */
	public static String talkToString(Talk talk) {
		if (talk.getLegth() == LIGHTNING_LENGTH)
			return talk.getTitle() + " " + LIGHTNING;
		else
			return talk.getTitle() + " " + talk.getLegth() + MIN;
	}
}
