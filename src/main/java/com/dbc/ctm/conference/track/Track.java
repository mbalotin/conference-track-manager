package com.dbc.ctm.conference.track;

import com.dbc.ctm.conference.Conference;
import com.dbc.ctm.conference.talk.Talk;
import com.dbc.ctm.exceptions.InvalidTalkLenghtException;

/**
 * Used to organize a specific set of {@link Talk}s in a {@link Conference}.
 * 
 * @author mbalotin
 *
 */
public class Track {

	protected final static int MORNING_SESSION_LENGTH = 3 * 60;
	protected final static int AFTERNOON_SESSION_LENGTH = 4 * 60;
	protected static final String NETWORK_EVENT_DESCRIPTION = "Networking Event";

	private Session morning, afternoon;

	public Track() {
		morning = new Session(MORNING_SESSION_LENGTH);
		afternoon = new Session(AFTERNOON_SESSION_LENGTH);
	}

	/**
	 * Schedules a {@link Talk} into an available spot into the {@link Track} .
	 * 
	 * @param talk
	 *            {@link Talk} to be scheduled.
	 * @return true - if the {@link Talk} were successfully scheduled </br>
	 *         false - if there is no available room for {@link Talk} in this
	 *         {@link Track}.
	 * @throws InvalidTalkLenghtException
	 *             If the {@link Talk} length exceeds the {@link Track} period size
	 */
	public boolean schedule(Talk talk) throws InvalidTalkLenghtException {
		// Tests if the Talk length does fit at least one of the Sessions.
		if (talk.getLegth() > MORNING_SESSION_LENGTH && talk.getLegth() > AFTERNOON_SESSION_LENGTH) {
			throw new InvalidTalkLenghtException(String.format(
					"Talk '%s' has a %d minutes duration and it is bigger than the Track Limit defined to %d minutes.",
					talk.getTitle(), talk.getLegth(), Math.max(MORNING_SESSION_LENGTH, AFTERNOON_SESSION_LENGTH)));
		}
		// adds either in morning, afternoon or none Session
		return morning.addTalk(talk) || afternoon.addTalk(talk);
	}

	/**
	 * Generate the schedule of this Track
	 * 
	 * @return a {@link StringBuilder} containing the concatenated information to be
	 *         printed. Decided to return the String Build due this information can
	 *         be used up in aggregation classes without wasting memory in
	 *         unnecessary String formation until final output.
	 */
	public StringBuilder printSchedule() {
		StringBuilder result = new StringBuilder();
		result.append(morning.printSchedule(8, 0, IntervalEnum.AM));
		result.append("12:00AM Lunch");
		result.append(System.getProperty("line.separator"));
		result.append(afternoon.printSchedule(1, 0, IntervalEnum.PM));
		/*
		 * As the Networking Event time constraints is a Track issue and not a Session
		 * one, its decided that the Network Event Scheduling is responsibility of the
		 * Track itself.
		 * 
		 */
		int hour = 1 + afternoon.getOccupation() / 60;
		int minutes = afternoon.getOccupation() % 60;
		// if the total occupation is earlier than 4:00 PM, set the Event to 4:00PM.
		if (hour < 4) {
			hour = 4;
			minutes = 0;
		}
		result.append(String.format("%02d:%02d%s %s", hour, minutes, IntervalEnum.PM, NETWORK_EVENT_DESCRIPTION));
		result.append(System.getProperty("line.separator"));
		return result;
	}

}
