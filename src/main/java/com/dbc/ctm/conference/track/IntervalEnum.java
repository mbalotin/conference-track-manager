package com.dbc.ctm.conference.track;

/**
 * Represents the Schedule Intervals to start the counting.
 * 
 * @author mbalotin
 * @see Session
 */
public enum IntervalEnum {
	AM, PM;
}
